import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this._loading = document.querySelector("progress");
    this._startLoading();
    this.emit(Application.events.READY);
  }

  async _render({ name, terrain, population }){
    let box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = `
      <article class="media">
        <div class="media-left">
          <figure class="image is-64x64">
            <img src="${image}" alt="planet">
          </figure>
        </div>
        <div class="media-content">
          <div class="content">
          <h4>${name}</h4>
            <p>
              <span class="tag">${terrain}</span> <span class="tag">${population}</span>
              <br>
            </p>
          </div>
        </div>
      </article>
    `;
    
    document.body.querySelector(".main").appendChild(box);
  }

  async _create(data){
    for (let index = 0; index < data.results.length; index++) {
      this._render({
        name: data.results[index].name,
        terrain: data.results[index].terrain,
        population: data.results[index].population,
      })
    }
  }

  async _load(url = "https://swapi.boom.dev/api/planets") {
    let response = (await fetch(url)
    .then((data) => {return data.json()}));
    
    await this._create(response);

    if (response.next !== null) {
      await this._load(response.next);
    }
    else{
      this._stopLoading();
    }
  }

  async _startLoading(){
    await this._load();
  }

  _stopLoading() {
    let progress = document.querySelector(".progress");
    progress.style.display = "none";
  }
}
